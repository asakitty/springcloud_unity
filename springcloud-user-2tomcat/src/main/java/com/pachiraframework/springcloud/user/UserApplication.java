package com.pachiraframework.springcloud.user;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.feign.EnableFeignClients;

/**
 * 打成war包，部署到tomcat上
 * 放入到tomcat中之后，spring boot 配置的端口号就没用了，会用tomcat的配置接口
 *
 *
 *部署springboot 到服务器上
 */
//@EnableFeignClients
//@EnableDiscoveryClient
@SpringBootApplication
public class UserApplication extends SpringBootServletInitializer {
	
	@Override  
    protected SpringApplicationBuilder configure(  
            SpringApplicationBuilder application) {  
        return application.sources(UserApplication.class);  
    }  
	
	public static void main(String[] args) {
		SpringApplication.run(UserApplication.class, args);
	}
}
