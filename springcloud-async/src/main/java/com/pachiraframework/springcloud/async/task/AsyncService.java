package com.pachiraframework.springcloud.async.task;


import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

@Service
public class AsyncService {
	
	
	@Async
    public String hello() throws Exception {
		
		
		Thread.sleep(10000);
		System.out.println(123);
		
		return "hello";
	}
	
	
	
	
}
