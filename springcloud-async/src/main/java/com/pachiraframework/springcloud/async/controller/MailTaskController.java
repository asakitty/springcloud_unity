package com.pachiraframework.springcloud.async.controller;

import java.io.File;
import java.util.concurrent.Future;

import javax.mail.internet.MimeMessage;

import org.apache.tomcat.jni.Mmap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.pachiraframework.springcloud.async.task.AsyncService;
import com.pachiraframework.springcloud.async.task.AsyncTasks;

/**
 * 测试异步任务Web控制器。
 *
 */
@RestController
public class MailTaskController {

    @Autowired
    JavaMailSender javaMailSender;
    /**
     * 测试异步任务。
     *localhost:10002/mail
     * @return
     * @throws Exception
     */
    @GetMapping("/mail")
    public String mail() throws Exception {
    	
    	
    	SimpleMailMessage message = new SimpleMailMessage();
    	
    	message.setSubject("通知，今晚开会");
    	message.setText("时间：今晚7:30");
    	message.setTo("1274060326@qq.com");
    	message.setFrom("2393868063@qq.com");
		javaMailSender.send(message);
    	
    	
        
        return "ok";
    }
    
    
    @GetMapping("/mailcomplix")
    public String mailcomplix() throws Exception {
    	
    	
    	
    	
    	MimeMessage message = javaMailSender.createMimeMessage();
    	MimeMessageHelper helper = new MimeMessageHelper(message,true);
    	
    	
    	
    	helper .setSubject("通知，今晚开会");
    	helper.setText("");//html
    	helper.setText("时间：今晚7:30");
    	helper.setTo("2393868063@qq.com");
    	helper.addAttachment("1.jpg",new File(""));//上传文件
    	helper.addAttachment("2.jpg",new File(""));//上传文件
		javaMailSender.send(message);
    	
    	
        
        return "ok";
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
}