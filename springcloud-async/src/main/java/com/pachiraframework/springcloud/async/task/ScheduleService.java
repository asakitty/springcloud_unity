package com.pachiraframework.springcloud.async.task;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

@Service
public class ScheduleService {

	/**
	 * 秒	分	时	日	月	周
	 * 
	 * 
	 * 总共6位，每位用空格分割
	 * 
	 * 0 * * * * MoN-SAT
	 */
//	@Scheduled(cron="0 * * * * MoN-SAT")
//	@Scheduled(cron="0,1,2,3,4 * * * * MoN-SAT")
//	@Scheduled(cron="0-4 * * * * MoN-SAT")
//	@Scheduled(cron="0/4 * * * * MoN-SAT")//每4秒执行一次
	public void hello(){
		System.out.println("...ScheduleService...");
	}
	
	
	
	
}
