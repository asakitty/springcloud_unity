package com.pachiraframework.springcloud.activemq.core;

/**
 * Created by jason-geng on 5/29/17.
 */
public interface Constant {

    String QUEUE_NAME = "ptp";

    String TOPIC_NAME = "pubsub";

    String RESPONSE_SUCCESS = "success";
    
    
    String QUEUE_CONTAINER = "ptpContainer";

    String TOPIC_CONTAINER = "pubsubContainer";
    
    
    
    
    
}
