package com.pachiraframework.springcloud.activemq.core;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 发布/订阅 - 调用生产者
 * Created by jason-geng on 5/21/17.
 */
@RestController
@RequestMapping(value = "/pubSub")
public class PubSubController {

    @Autowired
    private PubSubProducer pubSubProducer;
    //localhost:10101/pubSub/send
    @RequestMapping(value = "/send")
    public String send(){
        pubSubProducer.send();
        return Constant.RESPONSE_SUCCESS;
    }
    //localhost:10101/pubSub/convertAndSend
    @RequestMapping(value = "/convertAndSend")
    public String convertAndSend(){
        pubSubProducer.convertAndSend();
        return Constant.RESPONSE_SUCCESS;
    }
}
