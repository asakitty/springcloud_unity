package com.pachiraframework.springcloud.index.controller;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.annotation.Resource;

import org.apache.solr.common.SolrDocumentList;
import org.apache.solr.common.SolrInputDocument;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.pachiraframework.springcloud.index.bean.BaseResponse;
import com.pachiraframework.springcloud.index.bean.PushDataIntoSolrRequest;
import com.pachiraframework.springcloud.index.bean.QuerySolrIndexRequest;
import com.pachiraframework.springcloud.index.service.SolrService;

@RestController
@RequestMapping("/solr")
public class SolrController {  
      
    @Resource  
    private SolrService solrService;  
    /** 
     * 添加入solr索引  
     */  
//    @RequestMapping(value = "/pushDataIntoSolr" , method = RequestMethod.POST) 
    @RequestMapping(value="/push")
    @ResponseBody
    public void pushDataIntoSolr(@RequestBody PushDataIntoSolrRequest request){  
//        BaseResponse ress = new BaseResponse();  
//        SolrInputDocument input = new SolrInputDocument();  
//        Map<String, Object> asa = new HashMap<String, Object>();
//        asa.put("asa", "asa");
//        asa.put("asb", "asb");
//        asa.put("asc", "asc");
//        asa.put("asd", "asd");
//        Set<Entry<String, Object>> entrySet = asa.entrySet();
//        for (Map.Entry<String, Object> entry : entrySet) {  
//            input.addField(entry.getKey(), entry.getValue());  
//        }  
//        if (!solrService.pushDataIntoSolr(request.getCoreName(), input)){  
//            ress.setErrorCode("500");  
//            ress.setErrorMsg("插入失败，请稍候重试。");  
//        } else {  
//            ress.setSubMsg("请求成功，数据插入成功。");  
//        }  
        System.out.println("ok");
//        return ress.getSubMsg();  
    }  
      
    /** 
     * 按条件查询搜索引擎 
     */   
    @RequestMapping(value = "/querySolrIndex" , method = RequestMethod.GET)  
    public SolrDocumentList querySolrIndex(@RequestBody QuerySolrIndexRequest request){  
        return solrService.querySolrIndex(request.getCoreName(), request.getQuery());  
    }  
}  
