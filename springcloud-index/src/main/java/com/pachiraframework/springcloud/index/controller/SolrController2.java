package com.pachiraframework.springcloud.index.controller;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.annotation.Resource;

import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.SolrDocumentList;
import org.apache.solr.common.SolrInputDocument;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.pachiraframework.springcloud.index.bean.BaseResponse;
import com.pachiraframework.springcloud.index.service.SolrService;



@RestController
//@Controller
@RequestMapping("/solr2")
public class SolrController2 {
	@Resource  
	private SolrService solrService;  
	
	@RequestMapping(value= {"push"})
	@ResponseBody//一般说配置了@RestController，就ok了，但是如果不行就讲此@ResponseBody加入到此处
    //默认情况返回string会跳转到页面
	public Map<String, String> push() {
		
		BaseResponse ress = new BaseResponse();  
      SolrInputDocument input = new SolrInputDocument();  
      Map<String, Object> asa = new HashMap<String, Object>();
      asa.put("asa", "asa");
      asa.put("asb", "asb");
      asa.put("asc", "asc");
      asa.put("asd", "asd");
      Set<Entry<String, Object>> entrySet = asa.entrySet();
      for (Map.Entry<String, Object> entry : entrySet) {  
          input.addField(entry.getKey(), entry.getValue());  
      }  
      if (!solrService.pushDataIntoSolr("new_core", input)){ } 
//          ress.setErrorCode("500");  
//          ress.setErrorMsg("插入失败，请稍候重试。");  
//      } else {  
//          ress.setSubMsg("请求成功，数据插入成功。");  
//      }  
		
		
		
        Map<String, String> hashMap = new HashMap<>();
        hashMap.put("msg", "登录成功");
        return hashMap;
    }
	
	
	@RequestMapping(value= {"get"})
	@ResponseBody//一般说配置了@RestController，就ok了，但是如果不行就讲此@ResponseBody加入到此处
    //默认情况返回string会跳转到页面
	public Map<String, String> get() {
		SolrDocumentList querySolrIndex = solrService.querySolrIndex("new_core", "");
		System.out.println(querySolrIndex);
		Map<String, String> hashMap = new HashMap<>();
        hashMap.put("msg", "登录成功");
        return hashMap;
	}

}
