package com.pachiraframework.springcloud.index;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;
/**
 * spring boot +搜索引擎
 * 说句实在话吧，感觉挺难得，先写在此处，把想放在这里
 * 
 *
 */
//@EnableDiscoveryClient
@SpringBootApplication
public class LuceneNutchApplication {
	public static void main(String[] args) {
		SpringApplication.run(LuceneNutchApplication.class, args);
	}
}
