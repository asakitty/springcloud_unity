package com.pachiraframework.springcloud.index.service;

import org.apache.solr.common.SolrDocumentList;
import org.apache.solr.common.SolrInputDocument;

/** 
 *@ClassName:SolrService.java 
 *@ClassDescribe:操作solr 
 *@author:asa
 *@createDate:2017年9月28日 下午2:25:53 
 *@version 
 */  
public interface SolrService {  
      
    /** 
     *@ClassDescribe:向solr插入数据 
     *@author:chenxi 
     *@createDate:2017年9月28日 下午2:26:58 
     *@param coreName 核心名称 
     *@param input 数据封装 
     *@return 
     */  
    public boolean pushDataIntoSolr(String coreName ,SolrInputDocument input);  
      
    /** 
     *@ClassDescribe:按条件查询搜索引擎 
     *@author:chenxi 
     *@createDate:2017年9月28日 下午4:37:12 
     *@param query solr查询条件 
     *@return 返回查询集合 
     */  
    public SolrDocumentList querySolrIndex(String coreName, String query);  
      
}  
