//import java.util.List;
//import org.apache.spark.api.java.JavaRDD;
//import org.apache.spark.sql.Row;
//import org.apache.spark.sql.SparkSession;
//
//
//public class Bayes {
//
//	public Bayes(String file) {
//
//		SparkSession spark = SparkSession.builder()
//				.appName("Java Spark SQL data sources example")
//				.master("local[*]").config("master", "local[*]").getOrCreate();
//
//		//获取数据集
//		JavaRDD<Row> rdd = spark.read().parquet(file).toJavaRDD();
//		//获得数据集的文章列表
//		List<Row> records = rdd.collect();
//		BayesClassifier classifier = new BayesClassifier();
//		//训练数据集
//		classifier.train(records);
//		int corrects = 0;
//		for(Row record: records){
//			//文章实际所属的类别
//			String realClass = record.getString(4);
//			//分类的结果
//			String resultClass = classifier.classify(record);
//			//计算分类正确的数量
//			if(realClass.equals(resultClass)){
//				corrects++;
//			}
//		}
//		System.out.println("准确率: "+ (corrects * 1.0 / records.size()));
//	}
//
//	public static void main(String[] args) {
//		Bayes bayes = new Bayes("part.parquet");
//	}
//
//}
