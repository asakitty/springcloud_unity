package com.pachiraframework.springcloud.eureka.service.impl;


import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.pachiraframework.springcloud.eureka.dao.UserInfoDao;
import com.pachiraframework.springcloud.eureka.domain.UserInfo;
import com.pachiraframework.springcloud.eureka.service.UserInfoService;

@Service
public class UserInfoServiceImpl implements UserInfoService {
    @Resource
    private UserInfoDao userInfoDao;
    @Override
    public UserInfo findByUsername(String username) {
        System.out.println("UserInfoServiceImpl.findByUsername()");
        return userInfoDao.findByUsername(username);
    }
}