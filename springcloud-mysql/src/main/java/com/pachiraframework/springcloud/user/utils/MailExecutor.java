package com.pachiraframework.springcloud.user.utils;

import java.io.File;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.mail.MessagingException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 邮箱线程池
 * @author pengcq 412940226@qq.com	
 * @date 2017年8月7日 上午12:53:01
 */
public class MailExecutor {

	private MailUtils mailUtils;
	
	private static ExecutorService executorService = Executors
			.newFixedThreadPool(50);
	
	private final static Logger logger = LoggerFactory.getLogger(MailExecutor.class);
	
	public void sendMail(final String subject, final String text, final String[] sendTo,
			final Map<String, File> attachFile, final Map<String, File> inlineFile){
		executorService.execute(new Runnable() {
			public void run() {
				try {
					mailUtils.sendMail(subject, text, sendTo, attachFile, inlineFile);
				} catch (MessagingException e) {
					logger.error("发送邮件异常", e);
				}
				
			}
		});
	}
	
	public void sendTextMail(final String subject, final String text, final String[] sendTo){
		executorService.execute(new Runnable() {
			
			public void run() {
				mailUtils.sendTextMail(subject, text, sendTo);
				
			}
		});
	}
	
	public void sendHtmlMail(final String subject, final String text, final String[] sendTo){
		executorService.execute(new Runnable() {
			
			public void run() {
				try {
					mailUtils.sendHtmlMail(subject, text, sendTo);
				} catch (MessagingException e) {
					logger.error("发送邮件异常", e);
				}
				
			}
		});
	}
	
	public void sendHtmlMailWithAttachFile(final String subject, final String text,
			final String[] sendTo, final Map<String, File> attachFile){
		executorService.execute(new Runnable() {
			
			public void run() {
				try {
					mailUtils.sendHtmlMailWithAttachFile(subject, text, sendTo, attachFile);
				} catch (MessagingException e) {
					logger.error("发送邮件异常", e);
				}
				
			}
		});
	}
	
	public void sendHtmlMailWithInlineFile(final String subject, final String text,
			final String[] sendTo, final Map<String, File> inlineFile){
		executorService.execute(new Runnable() {
			
			public void run() {
				try {
					mailUtils.sendHtmlMailWithInlineFile(subject, text, sendTo, inlineFile);
				} catch (MessagingException e) {
					logger.error("发送邮件异常", e);
				}
				
			}
		});
	}

	public MailUtils getMailUtils() {
		return mailUtils;
	}

	public void setMailUtils(MailUtils mailUtils) {
		this.mailUtils = mailUtils;
	}
}
