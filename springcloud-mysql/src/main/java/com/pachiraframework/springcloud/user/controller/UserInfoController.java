package com.pachiraframework.springcloud.user.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.pachiraframework.springcloud.user.bean.User;
import com.pachiraframework.springcloud.user.service.UserInfoService;
import com.pachiraframework.springcloud.user.utils.MailUtils;



@Controller
@RequestMapping("/user")
public class UserInfoController {
	//@Autowired
	//private RestTemplate restTemplate;//通过外部链接获取数据的工具，这里没有用到
	@Autowired
	private UserInfoService userInfoService;
	
	@RequestMapping(value="index")
	public String index(Model model){
		model.addAttribute("title", "注册首页");
		return "user/index";
	}
	
	
	
	
	
	@RequestMapping(value= {"reg"},method=RequestMethod.POST)
	public String addone(Model model,@RequestParam("username")String username,@RequestParam("password")String password){
		User user=new User();
		user.setName(username);
		user.setAge(0);
		user.setPassword(password);
		userInfoService.saveUser(user);
		
		//上面的就是操作数据库的了，下面虽然与数据库无关，但是这种发送邮件的方法比较简洁，也写在这里吧
//		MailUtils mailUtils = new MailUtils();
//		try {
//			mailUtils.sendTextMail("我发邮件给你了", "哈哈", new String[]{"2393868063@qq.com"});
//		} catch (Exception e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
		return "user/success";
	}
	
	
	
	

}
