package com.pachiraframework.springcloud.user.dao.impl;

import org.nutz.dao.Cnd;
import org.nutz.dao.Dao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.pachiraframework.springcloud.user.bean.User;
import com.pachiraframework.springcloud.user.dao.UserDao;



@Repository("userDao")
public class UserDaoImpl implements UserDao{
	@Autowired private Dao dao;
	public User save(User user) {
		return dao.insert(user);
	}
	public void update(User user) {
		dao.update(user);
	}
	public User findByCode(String code) {
		return (User) dao.query(User.class, Cnd.where("code", "=", code));
	}

}
