package com.pachiraframework.springcloud.user.dao;

import com.pachiraframework.springcloud.user.bean.User;



public interface UserDao {

	public User save(User user);

	public void update(User user);

	public User findByCode(String code);

}
