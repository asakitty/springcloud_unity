package com.pachiraframework.springcloud.user.config;

import javax.sql.DataSource;

import org.nutz.dao.Dao;
import org.nutz.dao.impl.NutDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;
/**
 * nutz dao配置
 * @author pengcq 412940226@qq.com	
 * @date 2017年8月8日 下午10:50:24
 */
@Configuration
@Component("dao")
public class SnutzDao extends NutDao implements Dao{
	DataSource druidDataSource;
	@Autowired
	public void setDruidDataSource(DataSource druidDataSource) {
		this.druidDataSource = druidDataSource;
		setDataSource(druidDataSource);
	}
	
}
