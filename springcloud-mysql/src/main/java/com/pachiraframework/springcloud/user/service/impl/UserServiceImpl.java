package com.pachiraframework.springcloud.user.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.pachiraframework.springcloud.user.bean.User;
import com.pachiraframework.springcloud.user.dao.UserDao;
import com.pachiraframework.springcloud.user.service.UserInfoService;


@Service("userService")
public class UserServiceImpl implements UserInfoService {
	
	private static final Logger log = LoggerFactory.getLogger(UserServiceImpl.class);
	@Autowired private UserDao userDao;
	public User saveUser(User user) {
		try {
			return userDao.save(user);
		} catch (Exception e) {
			log.debug("error saveUser",e.getMessage());
		}
		return null;
		
	}
	public void update(User user) {
		userDao.update(user);
		
	}
	public User findByCode(String code) {
		return userDao.findByCode(code);
	}
	

}
