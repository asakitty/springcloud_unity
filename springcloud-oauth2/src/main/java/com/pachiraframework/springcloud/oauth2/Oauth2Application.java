package com.pachiraframework.springcloud.oauth2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

/**
 * 验证用户已经登录了
 * 
 * 
 * http://localhost:8888/oauth/authorize?response_type=code&client_id=client&redirect_uri=http://baidu.com&state=123
 * 
 * 点击Approve,跳转到baidu页面，后面携带了code和state参数
 *
 * https://www.baidu.com/?code=F7LsMB&state=123
 * 
 * 根据code换取access_code，注意使用post方法
 * code=F7LsMB这个属性是需要替换的 buffer：lxHiz9
 * http://localhost:8888/oauth/token?client_id=client&grant_type=authorization_code&redirect_uri=http://baidu.com&code=F7LsMB
 * 
 * 感觉这个例子没说明白，之后再找个例子吧，搞aop去了
 */
//@EnableDiscoveryClient
@SpringBootApplication
public class Oauth2Application extends WebMvcConfigurerAdapter {
	public static void main(String[] args) {
		SpringApplication.run(Oauth2Application.class, args);
	}

	//一种注册接口的方式，和用注解注册时一样的
	@Override
	public void addViewControllers(ViewControllerRegistry registry) {

		registry.addViewController("/login").setViewName("login");

		registry.addViewController("/oauth/confirm_access").setViewName(
				"authorize");

	}
}
