package com.pachiraframework.springcloud.zookeeper.service;

import org.springframework.stereotype.Service;

import com.alibaba.dubbo.config.annotation.Reference;

@Service
public class UserService {

	@Reference
	TicketService ticketService;
	
	public void hello(){
		String ticket = ticketService.getTicket();
		System.out.println("this is "+ticket);
	}
	
	
	
}
