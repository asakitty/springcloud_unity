package com.pachiraframework.springcloud.zookeeper.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.pachiraframework.springcloud.zookeeper.service.UserService;


@RestController
public class Controller {
	
	@Autowired
	UserService service;
	
	/**
	 * localhost:10010/tiket
	 * @param id
	 * @return
	 */
	@GetMapping("/tiket")
	public String tiket(){
		
		
		service.hello();
		
		return "ok";
	}
	
}
