package com.pachiraframework.springcloud.zookeeper;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 为了省事，这个的消费者和生产者全部都放在这里了
 * 
 * 1，引入依赖
 * 2，配置dubbo的注册中心的地址
 * 3，引用服务
 * 
 * 
 * 这个是要依赖另一个工程的，名字叫做springcloud-zookeeper，那边会将接口注册，这边直接使用接口即可了（但是要求接口类的路径报名名称是一致的）
 * 
 * 
 * @author asa
 *
 */
@SpringBootApplication
public class ZookeeperUserApplication {

	
	public static void main(String[] args) {
		SpringApplication.run(ZookeeperUserApplication.class, args);
	}
}
