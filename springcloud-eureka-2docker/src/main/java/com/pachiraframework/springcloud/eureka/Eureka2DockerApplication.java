package com.pachiraframework.springcloud.eureka;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;
/**
 * 
 * 没有成功
 * 在虚拟机上成功了，也不要想着在电脑上连接，太费事了,我错了，虚拟电脑其实可以连接的
 */
@EnableEurekaServer
@SpringBootApplication
public class Eureka2DockerApplication {
	public static void main(String[] args) {
		SpringApplication.run(Eureka2DockerApplication.class, args);
	}
}
