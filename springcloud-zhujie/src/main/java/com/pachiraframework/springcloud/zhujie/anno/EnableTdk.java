package com.pachiraframework.springcloud.zhujie.anno;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import com.pachiraframework.springcloud.zhujie.config.TdkConfig;

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE})
@Documented
@Import(TdkConfig.class)
//@Configuration//这个配置注解可要可不要
public @interface EnableTdk {

}
