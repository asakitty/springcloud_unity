package com.pachiraframework.springcloud.zhujie;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Import;

import com.pachiraframework.springcloud.zhujie.anno.EnableTdk;
import com.pachiraframework.springcloud.zhujie.config.TdkConfig;
import com.pachiraframework.springcloud.zhujie.domain.Sport;
import com.pachiraframework.springcloud.zhujie.domain.User;


@SpringBootApplication
@EnableTdk
//@Import(TdkConfig.class)
public class ZhuJieApplication {
	public static void main(String[] args) {
//		SpringApplication.run(ZhuJieApplication.class, args);
		
		
		ConfigurableApplicationContext context = SpringApplication.run(ZhuJieApplication.class, args);
		 
        //实现ImportBeanDefinitionRegistrar 实现Bean的注入
		
		//我们写的EnableTdk这个注解中，@Import将TdkConfig导入到框架之中了，在TdkConfig里面的属性都要用@Bean才能真的注入进来
        System.out.println(context.getBean(TdkConfig.class));
       
        System.out.println(context.getBean(User.class));
        
        System.out.println(context.getBean(Sport.class));

	}
}
