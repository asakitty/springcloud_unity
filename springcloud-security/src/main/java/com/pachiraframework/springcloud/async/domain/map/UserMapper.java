package com.pachiraframework.springcloud.async.domain.map;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.pachiraframework.springcloud.async.domain.User;


@Mapper
public interface UserMapper {

	@Select("select * from user where id=#{id}")
	public User getUserById(Integer id);
	
	
	@Update("update user set name=#{name},age=#{age},password=#{password} where id=#{id}")
	public void updateUserById(User user);
	
	@Delete("delete from user where id=#{id}")
	public void deleteUserById(Integer id);
	
	@Insert("insert into user(name,age,password) values(#{name},#{age},#{password})")
	public void insertUser(User user);
	
	
	@Select("select * from user where name=#{name}")
	public User getUserByName(String name);
	
	
}
