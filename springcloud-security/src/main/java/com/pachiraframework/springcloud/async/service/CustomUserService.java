package com.pachiraframework.springcloud.async.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;




import com.pachiraframework.springcloud.async.domain.User;
import com.pachiraframework.springcloud.async.domain.map.UserMapper;

public class CustomUserService implements UserDetailsService {
    @Autowired
//    UserRepository userRepository;
    UserMapper userMapper;
    
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
    	
    	User user = userMapper.getUserByName(username);
//        User user = userRepository.findByUsername(username);
        List<GrantedAuthority> authorities =new ArrayList<GrantedAuthority>();//赋权
        authorities.add(new SimpleGrantedAuthority("ROLE_ADMIN"));
        authorities.add(new SimpleGrantedAuthority("ROLE_vip1"));
        return new org.springframework.security.core.userdetails.User(user.getName(),user.getPassword(),authorities);
    }
}
