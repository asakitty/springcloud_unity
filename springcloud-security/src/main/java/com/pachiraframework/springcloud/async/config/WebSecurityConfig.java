package com.pachiraframework.springcloud.async.config;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.access.intercept.FilterSecurityInterceptor;

import com.pachiraframework.springcloud.async.service.CustomUserService;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
            .authorizeRequests()
                .antMatchers("/","/loginsubmit").permitAll()//都可以访问的路径配置
                .antMatchers("/level1/**").hasRole("vip1")
                .antMatchers("/level2/**").hasRole("vip2")
                .antMatchers("/level3/**").hasRole("vip3")
                .anyRequest().authenticated()
                .and()
            .formLogin()//开启自动配置的登录，上面的权限没有，就会跳转到这个页面
                .loginPage("/userlogin").usernameParameter("username").passwordParameter("password").successForwardUrl("/")
                .permitAll()
                .and()
            .logout()//自动配置的注销
                .logoutSuccessUrl("/");
        http
        	.csrf().disable();
        
        http.rememberMe().rememberMeParameter("remeberme"); 
    }

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        auth
            .inMemoryAuthentication()
                .withUser("user").password("password").roles("USER","vip1","vip2")//这种的就可以理解为写在应用中的用户
            .and()
            	.withUser("asa").password("asa").roles("vip3");
    }


    @Bean
    UserDetailsService customUserService(){
        return new CustomUserService();
    }
    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(customUserService());
    }
    @Override
    public void configure(WebSecurity web) throws Exception {
        super.configure(web);
    }
//    @Autowired
//    DataSource dataSource;
//    @Override
//    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
//        auth.jdbcAuthentication().dataSource(dataSource);
//    }
    
}