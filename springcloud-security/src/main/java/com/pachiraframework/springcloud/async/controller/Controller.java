package com.pachiraframework.springcloud.async.controller;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.pachiraframework.springcloud.async.domain.User;

@org.springframework.stereotype.Controller
public class Controller {

	
	/**
	 * localhost:20006
	 * @return
	 */
	@RequestMapping(value = { "/" })
	public String index(){
		return "index";
	}
	/**
	 * localhost:20006/login
	 * @return
	 */
	@RequestMapping(value = { "userlogin" })
	public String login(){
		return "userlogin";
	}
	
	@RequestMapping(value = { "loginsubmit" })
//	@PostMapping(value = { "loginsubmit" })
	public String loginsubmit(HttpServletRequest request,User user){
		Map<String, Object> map = new HashMap<>();  
        request.getSession().setAttribute("request Url", request.getRequestURL());  
        map.put("user", request.getRequestURL()); 
		
		
		return "index";
	}
	
	
	
	@RequestMapping(value = { "level1/1" })
	public String index1(){
		return "level1/1";
	}
	
	@RequestMapping(value = { "level1/2" })
	public String index2(){
		return "level1/2";
	}
	
	@RequestMapping(value = { "level1/3" })
	public String index3(){
		return "level1/3";
	}
	
	@RequestMapping(value = { "level2/4" })
	public String index4(){
		return "level2/4";
	}
	
	@RequestMapping(value = { "level2/5" })
	public String index5(){
		return "level2/5";
	}
	
	@RequestMapping(value = { "level2/6" })
	public String index6(){
		return "level2/6";
	}
	
	
	@RequestMapping(value = { "level3/7" })
	public String index7(){
		return "level3/7";
	}
	@RequestMapping(value = { "level3/8" })
	public String index8(){
		return "level3/8";
	}
	
	@RequestMapping(value = { "level3/9" })
	public String index9(){
		return "level3/9";
	}
	
}
