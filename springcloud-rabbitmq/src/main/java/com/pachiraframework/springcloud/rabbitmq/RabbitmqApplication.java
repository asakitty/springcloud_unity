package com.pachiraframework.springcloud.rabbitmq;

import org.springframework.amqp.rabbit.annotation.EnableRabbit;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.ConfigurationProperties;


/**
 * 自动配置
 * 1，RabbitAutoConfiguration
 * 2，CachingConnectionFactory连接工厂
 * 3，@ConfigurationProperties(prefix = "spring.rabbitmq")
 *		public class RabbitProperties
 * 4，RabbitTemplate,给RabbitMQ发送和接收信息
 * 5，AmqpAdmin，RabbitMQ系统管理功能组件
 * 		创建和删除，Queue，Exchange，Binding
 * 	
 * 6@EnableRabbit+@RabbitListener监听消息队列的内容
 * 
 * 
 * @author asa
 *
 */
@EnableRabbit//开启基于注解的Rabbit模式
@SpringBootApplication
public class RabbitmqApplication {
	public static void main(String[] args) {
		SpringApplication.run(RabbitmqApplication.class, args);
	}
}
