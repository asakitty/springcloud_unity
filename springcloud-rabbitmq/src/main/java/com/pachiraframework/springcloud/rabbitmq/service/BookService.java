package com.pachiraframework.springcloud.rabbitmq.service;

import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Service;

import com.pachiraframework.springcloud.rabbitmq.domain.Book;

@Service
public class BookService {

	@RabbitListener(queues="atguigu.news")
	public void receive(Book book){
		
		System.out.println("收到消息"+book.getName());
	}
	
	
	
	
	@RabbitListener(queues="atguigu")
	public void receive2(Message message){
		
		System.out.println("收到消息"+message.getBody());
	}
	
	
}
