package com.pachiraframework.springcloud.rabbitmq.config;

import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;



/**
 * 不搞这个配置，那么消息传输的过程中，会序列化封装，这个例子的配置是封装成json进行传输（序列化封装java代码获取消息是可以识别的，但要考虑跨语言的通讯）
 * @author asa
 *
 */
@Configuration
public class MyAMQPConfig {

	@Bean
	public MessageConverter messageConverter(){
		
		
		return new Jackson2JsonMessageConverter();
	}
	
	
}
