package com.pachiraframework.springcloud.rabbitmq.repository;

import java.util.List;

import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

import com.pachiraframework.springcloud.rabbitmq.domain.Book;

public interface BookRepository extends ElasticsearchRepository<Book, Integer>{
	
	
	
	public List<Book> findByBookNameLike(String bookName);
	
	
}
///web/htmlmode/update/{hmids}