package com.pachiraframework.springcloud.rabbitmq;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;



/**
 * 
 * springboot默认支持两种技术和es进行交互
 * 1，jest（默认不生效）需要导入jest的工具包，（io.searchbox.client.JestClient）
 * 2，springboot eiasticsearch{ES版本可能不适配}
 * 		1)Client客户端		节点信息clusterNodes,clusterName
 * 		2)ElasticsearchTemplate
 * 		3)编写一个ElasticsearchRepository的子接口来操作ES
 * 这里的版本是2.4.6
 * @author asa
 *
 */

@SpringBootApplication
public class ElasticsearchApplication {
	public static void main(String[] args) {
		SpringApplication.run(ElasticsearchApplication.class, args);
	}
}
