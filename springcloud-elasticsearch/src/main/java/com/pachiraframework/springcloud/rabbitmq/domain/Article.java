package com.pachiraframework.springcloud.rabbitmq.domain;

import io.searchbox.annotations.JestId;

public class Article {
	
	@JestId
	private Integer id;
	private String author;
	private String titles;
	private String content;
	
	
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) { 
		this.id = id;
	}
	public String getAuthor() {
		return author;
	}
	public void setAuthor(String author) {
		this.author = author;
	}
	public String getTitles() {
		return titles;
	}
	public void setTitles(String titles) {
		this.titles = titles;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	
	
	
	
	
	
	
	
	
}
