package com.pachiraframework.springcloud.cache.config;

import java.net.UnknownHostException;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.cache.RedisCacheManager;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.Jackson2JsonRedisSerializer;

import com.pachiraframework.springcloud.cache.domain.User;


@Configuration
public class MyRedisConfig {


	@Bean
	public RedisTemplate<Object, User> userRedisTemplate(
			RedisConnectionFactory redisConnectionFactory)
					throws UnknownHostException {
		RedisTemplate<Object, User> template = new RedisTemplate<Object, User>();
		template.setConnectionFactory(redisConnectionFactory);
		Jackson2JsonRedisSerializer<User> j2rs = new Jackson2JsonRedisSerializer<User>(User.class);
		template.setDefaultSerializer(j2rs);
		return template;
	}
	
	
	
	public RedisCacheManager userCacheManager(RedisTemplate<Object, User> userRedisTemplate){
		
		RedisCacheManager cacheManager = new RedisCacheManager(userRedisTemplate);
		cacheManager.setUsePrefix(true);//使用前缀，将cachename作为key的前缀
		
		return cacheManager;
	}
	
	
	
	
}
