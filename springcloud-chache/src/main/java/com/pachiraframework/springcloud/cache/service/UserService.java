package com.pachiraframework.springcloud.cache.service;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.stereotype.Service;

import com.pachiraframework.springcloud.cache.domain.User;
import com.pachiraframework.springcloud.cache.domain.map.UserMapper;

//@CacheConfig(cacheNames="user")//抽取缓存的公共配置
@Service
public class UserService {

	
	@Autowired
	UserMapper userMapper;
	
	/**
	 * 将方法的返回结果进行缓存，以后再要相同数据，直接从缓存中获取
	 * 
	 * CacheManager管理多个Cache组件的，对缓存的真正操作在Cache组件中，每一个缓存组件有自己的唯一一个名字；
	 * 
	 * 几个属性：
	 * 		cacheNames/value，指定缓存组件的名字
	 * 		key：缓存数据时使用的key，可以用它指定，默认是使用方法参数的值	1-方法返回值
	 * 			#id:参数id的值			
	 * 		keyGenerator：key的生成器，可以自己指定key的生成器的组件id
	 * 					key、keyGenerator二选一
	 * 		CacheManager：指定缓存管理器；或者cacheResolver指定获取解析器，也是二选一
	 * 		condition：指定符合条件的情况下才缓存
	 * 		unless：当	unless指定的条件为true，就不缓存	unless="#result==null"
	 * 		sync:是否使用异步模式
	 * 
	 * SimpleCacheConfiguration生效	
	 * 给容器中注册了一个CacheManager·ConcurrentMapCacheManager通过缓存的名字得到了一个对应关系
	 * 可以获取和创建ConcurrentMapCache类型的缓存组件，他的作用将数据保存在ConcurrentMap中
	 * 
	 * 运行的流程
	 * @Cacheable标注的方法执行之前先来检查缓存中有没有这个数据，默认按照参数的值作为key去查询，如果没有，
	 * 就运行方法 ，并将结果放入缓存中
	 * 
	 * #id参数的前面加#，就匹配到标签中了
	 * 
	 * 
	 * @param id
	 * @return
	 */
//	@Cacheable(cacheNames="user",key="#root.methodName+'['+#id+']'")
//	@Cacheable(cacheNames="user",keyGenerator="myKeyGenerator",condition="#a0>1 and #a0<10")
//	@Cacheable(cacheNames="user",keyGenerator="myKeyGenerator")
	@Cacheable(cacheNames="user",key="#id")
	public User getUserById(Integer id){
		
		System.out.println("getUserById:"+id);
		return userMapper.getUserById(id);
	}
	
	/**
	 * 即调用方法，又更新缓存
	 * 
	 * 先方法，后缓存
	 * @param user
	 * 
	 */
//	@CachePut(cacheNames="user",key="#user.id")
	@CachePut(cacheNames="user",key="#result.id")//这个是拿返回值，这个方法有返回，并且返回的对象就是放入更新的对象，所以两种方法都是可以的
	public User updateUserById(User user){
		System.out.println("updateUserById:"+user);
		userMapper.updateUserById(user);
		return user;
	}
	
	/**
	 * 缓存清除
	 * beforeInvocation属性，是否在方法程序运行之前来做这个清除缓存的操作
	 * 默认是在方法执行之后执行的 缓存清除 操作
	 * @param id
	 */
//	@CacheEvict(cacheNames="user",allEntries=true)//allEntries删除user所有的缓存记录
	@CacheEvict(cacheNames="user",key="#id")
	public void deleteUserById(Integer id){
		
		userMapper.deleteUserById(id);
	}
	
	
	
	
	@Caching(
			cacheable={
					@Cacheable(value="user",key="name")
			},
			put={
					@CachePut(value="user",key="#result.id"),
					@CachePut(value="user",key="#result.age")
			}
	)
	public User getUserByName(String name){
		
		
		return userMapper.getUserByName(name);
	}
	
	
	

	/**
	 * 插入一条数据
	 * @param user
	 */
	public void insertUser(User user){
		
		userMapper.insertUser(user);
		
	}
	
	
	
	
	
}
