package com.pachiraframework.springcloud.cache;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

/**
 * 简单点，只是调用缓存
 * 
 * 开启基于注解的缓存：@EnableCaching
 * 		@Cacheable
 * 		@CacheEvice
 * 		@CachePut
 * 		
 * 整合redis进行缓存
 * 引入redis的starter
 * 配置redis
 * 
 * 自定义cachemanager
 * @author asa
 *
 */
@MapperScan("com.pachiraframework.springcloud.cache.domain.map")
@SpringBootApplication
@EnableCaching
public class CacheApplication {
	public static void main(String[] args) {
		SpringApplication.run(CacheApplication.class, args);
	}
}
