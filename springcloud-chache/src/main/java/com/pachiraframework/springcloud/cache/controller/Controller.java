package com.pachiraframework.springcloud.cache.controller;




import java.io.IOException;
import java.util.List;












import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.ListOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.pachiraframework.springcloud.cache.domain.User;

import com.pachiraframework.springcloud.cache.service.UserService;


@RestController
public class Controller {

	@Autowired
	UserService userService;
	
	@Autowired
	StringRedisTemplate stringRedisTemplate;//操作的都是字符串
	
	@Autowired
	RedisTemplate redisTemplate;//k*v都是对象的
	
	@Autowired
	RedisTemplate userRedisTemplate;//k*v都是对象的
	
	/**
	 * localhost:20004/redistest
	 * redis的五大常用数据类型
	 * String字符串	list列表	 set集合	hash散列	zset有序集合
	 * @return
	 */
	@GetMapping("/redistest")
	public String redistest(){
//		ListOperations<String, String> opsForList = stringRedisTemplate.opsForList();
//		stringRedisTemplate.opsForValue().append("msg", "hello");//保存数据
//		String string = stringRedisTemplate.opsForValue().get("msg");
//		System.out.println(string);
		
//		stringRedisTemplate.opsForList().leftPush("mylist", "1");
//		stringRedisTemplate.opsForList().leftPush("mylist", "2");
//		stringRedisTemplate.opsForList().leftPush("mylist", "3");

		User user = new User();
		user.setName("asa");
		user.setAge(13);
		user.setId(111);
		user.setPassword("12345");
//		redisTemplate.opsForValue().set("user01", user);//序列化保存对象
		
		//将数据以json的方式进行保存
//		redisTemplate.opsForValue().set("user01", user);
		
		userRedisTemplate.opsForValue().set("user02", user);
		return "ok";
	}
	
	
	
	/**
	 * localhost:20004/cacheone/2  
	 * 
	 * @return
	 */
	@GetMapping("/cacheone/{id}")
	public User cacheone(@PathVariable("id") Integer id){
		
		
		User userById = userService.getUserById(id);
		System.out.println(userById);
		
		return userById;
	}
	
	
	/**
	 * localhost:20004/update/?name=asa&age=1&password=123&id=1
	 * @param user
	 * @return
	 */
	@GetMapping("/update")
	public User update(User user){
		
		User userById = userService.updateUserById(user);
		System.out.println(userById);
		
		return userById;
	}
	
	
	/**
	 * localhost:20004/delete/1
	 * @param user
	 * @return
	 */
	@GetMapping("/delete/{id}")
	public String delete(@PathVariable("id") Integer id){
		
		userService.deleteUserById(id);
		
		
		return "ok";
	}
	
	/**
	 * 
	 * @param name
	 * @return
	 */
	@GetMapping("/getUserByName/{name}")
	public User getUserByName(@PathVariable("name") String name){
		
		
		return userService.getUserByName(name);
	}
	
	
	
	
}
