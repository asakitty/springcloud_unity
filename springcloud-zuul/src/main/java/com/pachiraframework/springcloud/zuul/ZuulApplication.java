package com.pachiraframework.springcloud.zuul;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;
/**
 * 这个东西的作用就是这个了
 * http://localhost:8082/user/regist/sms?mobile=1234567895
 * 经过了zuul代理以后，我们就可以通过zuul的ip+端口来访问了
 * http://localhost:7777/user-service/user/regist/sms?mobile=1234567895
 * 其中user-service是user工程在服务器中注册的名字
 */
@EnableZuulProxy
@EnableDiscoveryClient
@SpringBootApplication
public class ZuulApplication {
	public static void main(String[] args) {
		SpringApplication.run(ZuulApplication.class, args);
	}
}
