package com.pachiraframework.springcloud.user.aop;

import java.util.Arrays;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

/****************************************************************************************
实现AOP的切面主要有以下几个要素：

使用@Aspect注解将一个java类定义为切面类
使用@Pointcut定义一个切入点，可以是一个规则表达式，比如下例中某个package下的所有函数，也可以是一个注解等。
根据需要在切入点不同位置的切入内容
使用@Before在切入点开始处切入内容
使用@After在切入点结尾处切入内容
使用@AfterReturning在切入点return内容之后切入内容（可以用来对处理返回值做一些加工处理）
使用@Around在切入点前后切入内容，并自己控制何时执行切入点自身的内容
使用@AfterThrowing用来处理当切入内容部分抛出异常之后的处理逻辑
使用@Order(i)注解来标识切面的优先级。i的值越小，优先级越高
****************************************************************************************/

/**
 * 其他两个是日志的输出，很有用，这个用来做session的缓存
 * 因为session在分布式中，一般是会从服务器中分离出来的，储存到缓存服务器中
 *所以在此用到aop来动态代理这个功能
 */
@Aspect
@Order(5)
@Component
public class SessionAspect {


   // private static final org.slf4j.Logger logger = LoggerFactory.getLogger(PreZuulFilter.class);

   private Logger logger = Logger.getLogger(getClass());

   ThreadLocal<Long> startTime = new ThreadLocal<>();

   private static final String PRE_TAG = "(Order(5))============== ";
   
   @Pointcut("execution(public * com.pachiraframework.springcloud.user.controller..*.*(..))")
   public void webLog(){}

   @Before("webLog()")
   public void doBefore(JoinPoint joinPoint) throws Throwable {
       startTime.set(System.currentTimeMillis());

       // 接收到请求，记录请求内容
       ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
       HttpServletRequest request = attributes.getRequest();
       
       

   }

   @AfterReturning(returning = "ret", pointcut = "webLog()")
   public void doAfterReturning(Object ret) throws Throwable {
       
   }
}