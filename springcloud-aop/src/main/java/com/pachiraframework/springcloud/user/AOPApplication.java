package com.pachiraframework.springcloud.user;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.feign.EnableFeignClients;

//@EnableFeignClients
//@EnableDiscoveryClient
/**
 * 只做核心的aop，其他配置就不放在这里了
 *
 */
@SpringBootApplication
public class AOPApplication {
	public static void main(String[] args) {
		SpringApplication.run(AOPApplication.class, args);
	}
}
