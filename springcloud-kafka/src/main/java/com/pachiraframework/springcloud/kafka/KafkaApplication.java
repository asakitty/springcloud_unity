package com.pachiraframework.springcloud.kafka;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

/**
 *spring cloud与kafka的交互
 *由于kafka需要zookeeper，而zookeeper需要jdk1.7，我没有安装1.7
 *就把spring cloud的实现写好吧，但没有测试
 *
 *
 */

/**
 *1）我没有介绍如何安装配置kafka，配置kafka时最好用完全bind网络ip的方式，而不是localhost或者127.0.0.1
 *2）最好不要使用kafka自带的zookeeper部署kafka，可能导致访问不通。
 *
 *3）理论上consumer读取kafka应该是通过zookeeper，但是这里我们用的是kafkaserver的地址，为什么没有深究。
 *
 *4）定义监听消息配置时，GROUP_ID_CONFIG配置项的值用于指定消费者组的名称，如果同组中存在多个监听器对象则只有一个监听器对象能收到消息。
 *
 *
 */

@SpringBootApplication
public class KafkaApplication {
	public static void main(String[] args) {
		SpringApplication.run(KafkaApplication.class, args);
	}
}
/*
这个是自主配置的，说明一下几个主要配置
#kafka依赖的zookeeper服务位置
kafka.consumer.zookeeper.connect=10.93.21.21:2181
#提交数据到kafka
kafka.consumer.servers=10.93.21.21:9092
kafka.consumer.enable.auto.commit=true
kafka.consumer.session.timeout=6000
kafka.consumer.auto.commit.interval=100
kafka.consumer.auto.offset.reset=latest
#主题
kafka.consumer.topic=test 
#分组
kafka.consumer.group.id=test
kafka.consumer.concurrency=10

#生产者服务端口
kafka.producer.servers=10.93.21.21:9092
kafka.producer.retries=0
kafka.producer.batch.size=4096
kafka.producer.linger=1
kafka.producer.buffer.memory=40960

*/
