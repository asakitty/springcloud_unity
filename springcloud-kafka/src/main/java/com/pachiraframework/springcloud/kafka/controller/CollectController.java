package com.pachiraframework.springcloud.kafka.controller;



//import com.kangaroo.sentinel.common.response.Response;
//import com.kangaroo.sentinel.common.response.ResultCode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.web.bind.annotation.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@RestController
@RequestMapping("/kafka")
public class CollectController {
    protected final Logger logger = LoggerFactory.getLogger(this.getClass());
    @Autowired
    private KafkaTemplate kafkaTemplate;

    @RequestMapping(value = "/send", method = RequestMethod.GET)
    @ResponseBody
    public String sendKafka(HttpServletRequest request, HttpServletResponse response) {
        try {
            String message = request.getParameter("message");
            logger.info("kafka的消息={}", message);
            kafkaTemplate.send("test", "key", message);
            logger.info("发送kafka成功.");
//            return new Response(ResultCode.SUCCESS, "发送kafka成功", null);
          return "发送kafka成功";

        } catch (Exception e) {
            logger.error("发送kafka失败", e);
//            return new Response(ResultCode.EXCEPTION, "发送kafka失败", null);
            return "发送kafka失败";
        }
    }

}