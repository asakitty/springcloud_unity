package com.pachiraframework.springcloud.user;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.feign.EnableFeignClients;
import org.springframework.scheduling.annotation.EnableScheduling;

//@EnableScheduling        //使能定时任务
@SpringBootApplication
public class SourceApplication {
	public static void main(String[] args) {
		SpringApplication.run(SourceApplication.class, args);
	}
}
