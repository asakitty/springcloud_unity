package com.pachiraframework.springcloud.user.schedul;

import javax.servlet.ServletContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.web.context.ContextLoader;
import org.springframework.web.context.WebApplicationContext;
  



/**
 * 
 *在定时器中获取到服务项目路径，操作项目下的文件
 */
@Component  
public class AnnotationQuartz {  
	@Autowired
    WebApplicationContext webApplicationContext;
  
	@Scheduled(fixedRate = 5000)  
    public void test() {  
		//下面这个方法失效了，采用上面注入的方法来做，且成功了
//    	WebApplicationContext webApplicationContext = ContextLoader.getCurrentWebApplicationContext();
    	ServletContext servletContext = webApplicationContext.getServletContext();
    	String realPath = servletContext.getRealPath("/");
    	
    	System.out.println("静态路径："+realPath);
    }  
  
    
      
  
}  
