package com.pachiraframework.springcloud.user.controller;

import java.util.HashMap;
import java.util.Map;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.ModelAndView;

import com.pachiraframework.springcloud.user.domain.User;

@Slf4j
@RestController
//@Controller
@RequestMapping("/source")
public class SourceController {
	@Autowired
	private RestTemplate restTemplate;
	
	@RequestMapping(value= {"json"})
	@ResponseBody//一般说配置了@RestController，就ok了，但是如果不行就讲此@ResponseBody加入到此处
    //默认情况返回string会跳转到页面
	public Map<String, String> json() {
        Map<String, String> hashMap = new HashMap<>();
        hashMap.put("msg", "登录成功");
        return hashMap;
    }
	@RequestMapping(value= {"asa"})
	public ModelAndView asa() {
        System.out.println("asa");
        return new ModelAndView("asa");
    }
	
	
	
	
	
	@RequestMapping(value = "/student", produces = {"application/xml", "application/json"})
	public Object sms(String mobile){
		
		return "xml";
	}
	
	
	

}
