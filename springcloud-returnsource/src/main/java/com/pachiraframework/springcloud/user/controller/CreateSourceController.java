package com.pachiraframework.springcloud.user.controller;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Map;

import org.apache.catalina.servlet4preview.http.HttpServletRequest;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.output.XMLOutputter;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
/**
 * 创建文件，并将文件保存在项目中的静态资源之中
 */
@RestController
@RequestMapping("/createsource")
public class CreateSourceController {
	@RequestMapping(value= {"xml"})
	@ResponseBody
	public String json(HttpServletRequest request) {
        String filePath = request.getSession().getServletContext().getRealPath("/");
		try {
			BuildXMLDoc(filePath);
		} catch (IOException | JDOMException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "ok";
	}
	
	
	
	
	public void BuildXMLDoc(String filePath) throws IOException, JDOMException {

		// 创建根节点 list;
		Element root = new Element("urlset");
		root.setAttribute("content_method", "full");
		// 根节点添加到文档中；
		Document Doc = new Document(root);

		for (int i = 0; i < 1; i++) {
			Element url = new Element("url");
			url.addContent(new Element("loc").setText("http://baidu.com"));
			Element data = new Element("data");
			Element display = new Element("display");
			display.addContent(new Element("type").setText("租房"));
			display.addContent(new Element("provider").addContent(new Element("brand").setText("新环境房屋网")));
			Element rentInfo = new Element("rentInfo");
			rentInfo.addContent(new Element("name").setText("融泽嘉园 精装两居 高间楼层"));
			rentInfo.addContent(new Element("url").setText("http://www.baidu.com"));
			rentInfo.addContent(new Element("wapUrl").setText("http://m.baidu.com"));
			Element image = new Element("image");
			image.addContent(new Element("contentUrl").setText("http://xxx.xxx.com/xxx.jpg"));
			rentInfo.addContent(image);
			rentInfo.addContent(new Element("decoration").setText("简装"));
			rentInfo.addContent(new Element("rentType").setText("整租"));
			rentInfo.addContent(new Element("price").setText("2300"));
			display.addContent(rentInfo);
			display.addContent(new Element("houseStructureName").setText("2室1厅"));
			display.addContent(new Element("houseStructureShi").setText("2"));
			display.addContent(new Element("houseArea").setText("98.01"));
			display.addContent(new Element("category").setText("住宅"));
			display.addContent(new Element("weight").setText("700"));
			Element neighbourhood = new Element("neighbourhood");
			neighbourhood.addContent(new Element("name").setText("融泽嘉园"));
			Element addressSpecification = new Element("addressSpecification");
			addressSpecification.addContent(new Element("administrativeAreaLv2").setText("北京"));
			neighbourhood.addContent(addressSpecification);
			display.addContent(neighbourhood);
			display.addContent(new Element("id").setText("12345"));
			data.addContent(display);
			url.addContent(data);
			root.addContent(url);

		}
		XMLOutputter XMLOut = new XMLOutputter();

		// 输出 user.xml 文件；
		XMLOut.output(Doc, new FileOutputStream(filePath+"user.xml"));
		
	}

}
