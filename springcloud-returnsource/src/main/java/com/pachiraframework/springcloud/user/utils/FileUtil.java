package com.pachiraframework.springcloud.user.utils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;


import org.apache.tomcat.util.http.fileupload.disk.DiskFileItem;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.commons.CommonsMultipartFile;




public class FileUtil {
	public static void uploadFile(byte[] file, String filePath, String fileName) throws Exception { 
        File targetFile = new File(filePath);  
        if(!targetFile.exists()){    
            targetFile.mkdirs();    
        }       
        FileOutputStream out = new FileOutputStream(filePath+fileName);
        out.write(file);
        out.flush();
        out.close();
    }
	
	
	/** 
     * MultipartFile 转换成File 
     *  真是日了狗了，还是要先存在本地然后再取出File对象
     * @param multfile 原文件类型 
     * @return File 
     * @throws IOException 
     */  
    public static File multipartToFile(MultipartFile multfile) throws IOException {  
//    	MultipartFile file = multfile;  
//    	CommonsMultipartFile cf= (CommonsMultipartFile)file;  
//    	DiskFileItem fi = (DiskFileItem)cf.getFileItem();  
    	File f = new File("D:\\xml\\asa.jpg");  
    	multfile.transferTo(f);
        return f;  
    }  
	
	
}
