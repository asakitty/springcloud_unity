package com.pachiraframework.springcloud.zookeeper;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


/**
 * 为了省事，这个的消费者放在这里了
 * 1,引入dubbo和zookeeper的zkclient的相关依赖
 * 2，配置dubbo的扫描包和注册中心地址
 * 3，使用@Service来发布
 * 
 * @author asa
 *
 */
@SpringBootApplication
public class ZookeeperApplication {
	public static void main(String[] args) {
		SpringApplication.run(ZookeeperApplication.class, args);
	}
}
