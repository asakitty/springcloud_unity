package com.pachiraframework.springcloud.eureka.page;

import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * 订单查询条件
 * 
 * @author Administrator
 *
 */
public class PayCondition {

	private String belongerInfo;//经纪人姓名,工号或手机号码

	private Integer type;//1:推广, 2:预约刷新, 3:刷新

	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date createDateFrom;
//	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date createDateTo;

	private String deputyId; //副总id

	private String majordomoId; //总监id

	private String storefrontId; //门店id

	private String storegroupId; //店组id

	public String getDeputyId() {
		return deputyId;
	}

	public void setDeputyId(String deputyId) {
		this.deputyId = deputyId;
	}

	public String getMajordomoId() {
		return majordomoId;
	}

	public void setMajordomoId(String majordomoId) {
		this.majordomoId = majordomoId;
	}

	public String getStorefrontId() {
		return storefrontId;
	}

	public void setStorefrontId(String storefrontId) {
		this.storefrontId = storefrontId;
	}

	public String getStoregroupId() {
		return storegroupId;
	}

	public void setStoregroupId(String storegroupId) {
		this.storegroupId = storegroupId;
	}

	public String getBelongerInfo() {
		return belongerInfo;
	}

	public void setBelongerInfo(String belongerInfo) {
		this.belongerInfo = belongerInfo;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public Date getCreateDateFrom() {
		return createDateFrom;
	}

	public void setCreateDateFrom(Date createDateFrom) {
		this.createDateFrom = createDateFrom;
	}

	public Date getCreateDateTo() {
		return createDateTo;
	}

	public void setCreateDateTo(Date createDateTo) {
		this.createDateTo = createDateTo;
	}

	@Override
	public String toString() {
		return "PayCondition [belongerInfo=" + belongerInfo + ", type=" + type
				+ ", createDateFrom=" + createDateFrom + ", createDateTo="
				+ createDateTo + ", deputyId=" + deputyId + ", majordomoId="
				+ majordomoId + ", storefrontId=" + storefrontId
				+ ", storegroupId=" + storegroupId + "]";
	}
	
}
