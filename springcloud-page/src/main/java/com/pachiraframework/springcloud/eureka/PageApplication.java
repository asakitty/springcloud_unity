package com.pachiraframework.springcloud.eureka;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@SpringBootApplication
public class PageApplication {
	public static void main(String[] args) {
		SpringApplication.run(PageApplication.class, args);
	}
}
