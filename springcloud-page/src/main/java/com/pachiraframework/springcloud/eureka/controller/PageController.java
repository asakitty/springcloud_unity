package com.pachiraframework.springcloud.eureka.controller;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mongodb.DBObject;
import com.pachiraframework.springcloud.eureka.page.PayCondition;
import com.pachiraframework.springcloud.eureka.service.PayService;
import com.pachiraframework.springcloud.eureka.util.ExcelExport;


@Controller
//@RequestMapping(value = "/page")
public class PageController {
	PayService payService; 
	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

	@RequestMapping("/list")
	public String list(ModelMap model, PayCondition condition, Integer page, Integer size) {
		queryData(model, condition, page, size);
		System.out.println("/admin/pay/list");
		return "/pay/list";
	}

	@RequestMapping("/pagination")
	public String pagination(ModelMap model, PayCondition condition, Integer page, Integer size) {
		queryData(model, condition, page, size);
		System.out.println("/admin/pay/pagination");
		return "/pay/pagination";
	}

	public void queryData(ModelMap model, PayCondition condition, Integer page, Integer size) {
		if (page == null || page < 0) {
			page = 0;
		}
		if (size == null) {
			size = 10;
		}
		Pageable pageable = new PageRequest(page, size);
		System.out.println(condition);
		Page<DBObject> datas = payService.findByCondition(condition, pageable);
		
		datasList = payService.findListByCondition(condition);

		Map<String, Double> map = payService.findMoneyCount(condition);
		if(map!=null){
			model.addAttribute("count", map.get("count"));
			model.addAttribute("buyCount", map.get("buyCount"));
			model.addAttribute("giveCount", map.get("giveCount"));
		}
		model.addAttribute("datas", datas);
		model.addAttribute("condition", condition);
	}
	List<DBObject> datasList;
	@RequestMapping("/exportExcel")
	@ResponseBody
	public String exportExcel(HttpServletResponse response){

		List<String> name = new ArrayList<>();
		name.add("姓名");
		name.add("工号");
		name.add("电话");
		name.add("充值时间");
		name.add("充值金额(￥)");
		name.add("获得新币");

		List<List<String>> listValue = new ArrayList<>();

		for (DBObject dbObject: datasList) {
			List<String> list1 = new ArrayList<>();
			list1.add(dbObject.get("belongerName")+"");
			list1.add(dbObject.get("belongerJobNum")+"");
			list1.add(dbObject.get("belongerPhone")+"");
			list1.add(sdf.format(dbObject.get("createDate"))+"");
			list1.add(Double.valueOf(dbObject.get("amount").toString())/100+"");
			list1.add("冲"+dbObject.get("buy")+",赠"+dbObject.get("give"));
			listValue.add(list1);
		}

		Boolean result = ExcelExport.exportExcel(response, "充值记录" ,name, listValue);
		if(result){
			return "success";
		}else {
			return "fail";
		}
	}
	
	
	
}
