package com.pachiraframework.springcloud.eureka.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import com.mongodb.DBObject;
import com.pachiraframework.springcloud.eureka.page.PayCondition;

public class PayService {
/**
 * <td class="text-center">${(d.belongerName)!}</td>
	<td class="text-center">${(d.belongerJobNum)!}</td>
	<td class="text-center">${(d.belongerPhone)!}</td>
	<td class="text-center">${d.createDate?datetime}</td>
	<td class="text-center">${(d.amount/100)!}</td>
	<td class="text-center">${(d.buy)!}+${(d.give)!}</td>
 */
	public Page<DBObject> findByCondition(PayCondition condition,
			Pageable pageable) {
		List<DBObject> content = new ArrayList<DBObject>();
		for (int i = 0; i < 10; i++) {
			content.get(i).put("belongerName", i);
			content.get(i).put("belongerJobNum", i);
			content.get(i).put("belongerPhone", i);
			content.get(i).put("createDate", i);
			content.get(i).put("amount", i);
			content.get(i).put("buy", i);
		}
		
		
		
		Page<DBObject> asa = new PageImpl<DBObject>(content);
		
		
		return asa;
	}

	public List<DBObject> findListByCondition(PayCondition condition) {
		List<DBObject> content = new ArrayList<DBObject>();
		for (int i = 0; i < 10; i++) {
			content.get(i).put("belongerName", i);
			content.get(i).put("belongerJobNum", i);
			content.get(i).put("belongerPhone", i);
			content.get(i).put("createDate", i);
			content.get(i).put("amount", i);
			content.get(i).put("buy", i);
		}
		
		
		return content;
	}

	public Map<String, Double> findMoneyCount(PayCondition condition) {
		/**
		 * model.addAttribute("count", map.get("count"));
			model.addAttribute("buyCount", map.get("buyCount"));
			model.addAttribute("giveCount", map.get("giveCount"));
		 */
		Map<String, Double> asa = new HashMap<String, Double>();
		asa.put("count", 100.0);
		asa.put("buyCount", 100.0);
		asa.put("giveCount", 100.0);
		return asa;
	}

}
