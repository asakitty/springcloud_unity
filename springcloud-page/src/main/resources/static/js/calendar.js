function toVaild() {
	var datefrom = $("#dateFrom").val().trim();
	var dateto = $("#dateTo").val().trim();
	var reg = /(^\d{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2]\d|3[0-1])$)|(^\s*$)/
	if (reg.test(datefrom) && reg.test(dateto)) {
		if (dateto) {
			$("#dateTo").val(dateto + ' 23:59:59');
		}
		return true;
	} else {
		alert("请填写正确的日期格式");
		return false;
	}
}
$('.datetimepicker').datetimepicker({
	language : 'zh-CN',
	weekStart : 1,
	todayBtn : 1,
	autoclose : 1,
	todayHighlight : 1,
	startView : 2,
	minView : 2,
	forceParse : 0,
	format : 'yyyy-mm-dd'
});

 

