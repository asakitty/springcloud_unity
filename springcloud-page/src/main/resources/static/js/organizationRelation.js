$(function() {
    $("#organization82").html("");
    var deputyId = $("#deputyId").val();
    $.ajax({
        url: "/admin/organization/getDepartment",
        type: "GET",
        data:{},
        dataType: "JSON",
        success: function (data) {
            if(deputyId != ""){
                $("#organization82").append("<option value=''>"+"不限"+"</option>");
            }else{
                $("#organization82").append("<option selected = 'selected' value=''>"+"不限"+"</option>");
            }
            for (var i = 0; i < data.length; i++) {
                if(data[i].id == deputyId) {
                    $("#organization82").append("<option selected = 'selected' value='" + data[i].id + "'>" + data[i].departmentName + "</option>");
                }else{
                    $("#organization82").append("<option value='" + data[i].id + "'>" + data[i].departmentName + "</option>");
                }
            }
            selectDeputy();
        }
    })
})

function selectDeputy() {
    $("#organization83").html("");
    var majordomoId = $("#majordomoId").val();
    var deputyId = $("#organization82").val();
    if(deputyId == ""){
        $("#organization83").append("<option selected = 'selected' value=''>不限</option>");
        selectMajordomo();
    }else {
        $.ajax({
            url: "/admin/organization/getDepartment",
            type: "GET",
            data: {"parentId": deputyId},
            dataType: "JSON",
            success: function (data) {
                if(majordomoId != ""){
                    $("#organization83").append("<option value=''>"+"不限"+"</option>");
                }else{
                    $("#organization83").append("<option selected = 'selected' value=''>"+"不限"+"</option>");
                }
                for (var i = 0; i < data.length; i++) {
                    if (data[i].id == majordomoId) {
                        $("#organization83").append("<option selected = 'selected' value='" + data[i].id + "'>" + data[i].departmentName + "</option>");
                    } else {
                        $("#organization83").append("<option value='" + data[i].id + "'>" + data[i].departmentName + "</option>");
                    }
                }
                selectMajordomo();
            }
        })
    }
}

function selectMajordomo() {
    $("#organization85").html("");
    var storefrontId = $("#storefrontId").val();
    var majordomoId = $("#organization83").val();
    if(majordomoId == ""){
        $("#organization85").append("<option selected = 'selected' value=''>不限</option>");
        selectStorefront();
    }else {
        $.ajax({
            url: "/admin/organization/getDepartment",
            type: "GET",
            data: {"parentId": majordomoId},
            dataType: "JSON",
            success: function (data) {
                if(storefrontId != ""){
                    $("#organization85").append("<option value=''>"+"不限"+"</option>");
                }else{
                    $("#organization85").append("<option selected = 'selected' value=''>"+"不限"+"</option>");
                }
                for (var i = 0; i < data.length; i++) {
                    if (data[i].id == storefrontId) {
                        $("#organization85").append("<option selected = 'selected' value='" + data[i].id + "'>" + data[i].departmentName + "</option>");
                    } else {
                        $("#organization85").append("<option value='" + data[i].id + "'>" + data[i].departmentName + "</option>");
                    }
                }
                selectStorefront();
            }
        })
    }
}

function selectStorefront() {
    $("#organization88").html("");
    var storegroupId = $("#storegroupId").val();
    var storefrontId = $("#organization85").val();
    if(storefrontId == ""){
        $("#organization88").append("<option selected = 'selected' value=''>不限</option>");
        //changeOrganization88($("#organization83").val());
    }else {
        $.ajax({
            url: "/admin/organization/getDepartment",
            type: "GET",
            data: {"parentId": storefrontId},
            dataType: "JSON",
            success: function (data) {
                if(storefrontId != ""){
                    $("#organization88").append("<option value=''>"+"不限"+"</option>");
                }else{
                    $("#organization88").append("<option selected = 'selected' value=''>"+"不限"+"</option>");
                }
                for (var i = 0; i < data.length; i++) {
                    if (data[i].id == storegroupId) {
                        $("#organization88").append("<option selected = 'selected' value='" + data[i].id + "'>" + data[i].departmentName + "</option>");
                    } else {
                        $("#organization88").append("<option value='" + data[i].id + "'>" + data[i].departmentName + "</option>");
                    }
                }
            }
        })
    }
}
