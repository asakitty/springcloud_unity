var Pager = function(page,totalPages,result,pagination){
    this.currPage = page;//当前页
	this.totalPages = totalPages;//总页数
	this.result = result;//查询结果
	this.page = pagination;//分页条
}

//页面跳转
function goPage(pageNo,url) {
	console.log(1)
    $("#"+pager.currPage).val(pageNo);
    pagination(pageNo, $("#"+pager.totalPages).val(),url);
    getData(url);
}

//分页查询结果页面
function getData(url){
    var params = $("form:first").serialize();
    $.ajax({
        type: "POST",
        url: url,
        data: params,
        success: function(html){
          $("#"+pager.result).html(html);
        }
     });
}



// 显示页码
function pagination(curPage, totalPage,url) {
    url = "'"+url+"'";
    var sHtml = '<ul class="pagination">';
    if(totalPage>0){
        sHtml += '<div class="pull-left mr10 line32">共'+totalPage+'页&nbsp;</div><li class="paginate_button"><a type="button" style="cursor:pointer" name="skip" onclick="pageskip('+url+','+totalPage+');">跳到第</a></li>';
        sHtml += '<input class="form-control w55 pull-left" type="text" id="pageskip" name="pageskip" size="2" onkeyup="this.value=this.value.replace(/\\D/gi,'+"'"+"'"+')"><div class="pull-left mr10 line32">页&nbsp;&nbsp;&nbsp;</div>';
    }
    if(curPage > 0) {
        sHtml += '<li class="paginate_button previous"><a href="javascript:goPage(0,'+url+');">首页</a></li><li class="paginate_button"><a href="javascript:goPage('+(curPage - 1)+','+url+');">上一页</a></li>';
    }
    if(totalPage < 10) {
        for (var i = 0; i < totalPage; i++) {
            if(i == curPage){
                sHtml += '<li class="paginate_button active"><a href="javascript:goPage(' + i + ','+url+');">'+(i+1)+'</a></li>';
            }else{
                sHtml += '<li class="paginate_button "><a href="javascript:goPage(' + i + ','+url+');">'+(i+1)+'</a></li>';
            }
        }
    } else {
        if(curPage < 5) {
            for(var i = 0; i < 10; i++) {
                if(i == curPage){
                    sHtml += '<li class="paginate_button active"><a href="javascript:goPage(' + i + ','+url+');">'+(i+1)+'</a></li>';
                }else{
                    sHtml += '<li class="paginate_button "><a href="javascript:goPage(' + i + ','+url+');">'+(i+1)+'</a></li>';
                }
            }
        } else if(totalPage - curPage < 4) {
            for(var i = totalPage - 8; i < totalPage; i++) {
                if(i == curPage){
                    sHtml += '<li class="paginate_button active"><a href="javascript:goPage(' + i + ','+url+');">'+(i+1)+'</a></li>';
                }else{
                    sHtml += '<li class="paginate_button "><a href="javascript:goPage(' + i + ','+url+');">'+(i+1)+'</a></li>';
                }
            }
        } else {
            for(var i = curPage - 4; i < parseInt(curPage) + 5; i++){
                if(i == curPage){
                    sHtml += '<li class="paginate_button active"><a href="javascript:goPage(' + i + ','+url+');">'+(i+1)+'</a></li>';
                }else{
                    sHtml += '<li class="paginate_button "><a href="javascript:goPage(' + i + ','+url+');">'+(i+1)+'</a></li>';
                }
            }  
        }  
    }
    if(curPage < (totalPage-1)) {
        sHtml += '<li class="paginate_button"><a href="javascript:goPage('+(parseInt(curPage) + 1)+','+url+');">下一页</a></li><li class="paginate_button next"><a href="javascript:goPage('+ (totalPage-1) +','+url+');">尾页</a></li>';
    }
    sHtml +='</ul>';
    $("#"+pager.page).html(sHtml);
}

function pageskip(url,totalPage){
	var pageskip = $("#pageskip").val();
	var i = parseInt(pageskip)-1;
	if(pageskip==""){
		i=0;
		goPage(i,url)
	}else if(i>=totalPage){
		goPage(parseInt(totalPage)-1,url)
	}else{
		goPage(i,url)
	}
}